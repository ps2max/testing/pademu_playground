all:
	$(MAKE) -C pademu all USE_USB=1
	$(MAKE) -C pademu all USE_BT=1
	$(MAKE) -C sample_padx all

clean:
	$(MAKE) -C pademu clean USE_USB=1
	$(MAKE) -C pademu clean USE_BT=1
	$(MAKE) -C sample_padx clean

format:
	find . -type f -not -path \*modules/network/SMSTCPIP\* -a \( -iname \*.h -o -iname \*.c \) | xargs clang-format -i

copy:
	cp $(PS2SDK)/iop/irx/*.irx modules/

reset: all
	ps2client -h 192.168.1.10 reset

run: all
	ps2client -h 192.168.1.10 execee host:sample_padx/padx_sample.elf
